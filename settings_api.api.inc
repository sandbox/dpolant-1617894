<?php

/**
 * Defines variable settings and adds them as entity metadata properties. If 
 * applicable, tokenization will be applied.
 * 
 * Return an array keyed by variable names with elements: 
 * - default value: Default value for variable
 * - type: Any scalar type (text, integer, uri, etc). 
 *     @see hook_entity_property_info().
 * - label: Variable label
 * - description: Variable description
 * - attach: Specify which entity metadata element the variable should be 
 *     attached to, e.g. "site", "node", etc.
 * - token variables: List of token variable definitions, with keys "type" (as 
 *     in entity metadata types), "label", and "description"
 * - token data callback: If variable property is tokenizable, provide a
 *     callback that maps attached entity data to the different token variable
 *     values. @see module_line_item_token_callback().
 * 
 * @return array
 */
function hook_settings_api_variable_properties() {
  $token_vars = array(
    'site' => array(
      'type' => 'site',
      'label' => t('Site information'),
    ),
    'line_item' => array(
      'label' => t('Line item'),
      'type' => 'commerce_line_item',
    ),
    'order' => array(
      'label' => t('Order'),
      'type' => 'commerce_order',
    ),
    'user' => array(
      'label' => t('Order owner'),
      'type' => 'user',
    ),
    'product' => array(
      'label' => t('Product'),
      'type' => 'commerce_product'
    )
  );
  
  return array(
    'variable_name_1' => array(
      'default value' => 0,
      'label' => t('Label'),
      'description' => t('Description'),
      'attach' => 'line_item',
      'token variables' => $token_vars,        
      'token data callback' => 'module_line_item_token_callback',        
      'type' => 'text'
    )
  );
}

/**
 * Sample token value callback. In this case $data is a line item since we are
 * attaching the variable to the line item. Global token variables like "site" 
 * do not need to be accounted for.
 * 
 * @param mixed $data
 *  The data passed through settings_api_entity_property_getter() which is of
 *  the type defined in the "attach" element of hook_settings_api_variable_properties().
 * 
 * @param string $token_var_name
 *  Name of the token variable, corresponds to keys of "token variables element
 *  of hook_settings_api_variable_properties().
 * 
 * @param array $definition 
 *  Full definition of the variable properties
 */
function module_line_item_token_callback($data, $token_var_name, $definition) {
  $wrapper = entity_metadata_wrapper('commerce_line_item', $data);
  
  switch ($token_var_name) {
    case 'line_item':
      return $data;
    case 'order':
      return $wrapper->order->value();
    case 'user':
      return $wrapper->order->owner->value();
    case 'product':
      return $wrapper->commerce_product->value();      
  }
}