<?php

/*
 * Form callback: administrative mail config
 */
function settings_api_mail_admin_form() {
  $form = array();
  $mail_types = settings_api_mail_mail_types();
  
  if (empty($mail_types)) {
    drupal_set_message(t('No mail types defined.'));
    return array();
  }
  
  $form['notifications'] = array(
    '#type' => 'vertical_tabs',
  );

  foreach ($mail_types as $name => $type) {
    // Display replacements guide
    $token_var_info = $mail_types[$name]['token variables'];
    $token_help = RulesTokenEvaluator::help($token_var_info);
    
    $setting_name = 'settings_api_mail_' . $name;

    // Build form for this type
    $container_key = $name . '_container';
    $form[$container_key] = array(
      '#type' => 'fieldset',
      '#title' => isset($type['title']) ? $type['title'] : $name,
      '#description' => isset($type['description']) ? $type['description'] : '',
      '#attributes' => array('class' => array()),
      '#group' => 'notifications',
    );

    $form[$container_key]["{$setting_name}_subject"] = array(
      '#type' => 'textfield',
      '#title' => t('Subject'),
      '#default_value' => variable_get("{$setting_name}_subject", ''),
      '#maxlength' => 180,
    );

    $form[$container_key]["{$setting_name}_body"] = array(
      '#type' => 'textarea',
      '#title' => t('Body'),
      '#default_value' => variable_get("{$setting_name}_body", ''),
      '#rows' => 15,
    );
    $form[$container_key]['notifications_token_help'] = $token_help;
    $form[$container_key]['notifications_token_help']['#type'] = 'container';
  }
  
  return system_settings_form($form);
}
