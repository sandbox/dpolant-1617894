<?php

/**
 * Define mail template types
 * 
 * All mail template types get stored as variables: one for subject and one for
 * body. Most of the information defined here gets split into subject/body and 
 * then passed through to hook_settings_api_variable_properties().
 * 
 * - weight: define display order for templates
 * 
 * All other variables: @see hook_settings_api_variable_properties().
 * @return type 
 */
function hook_settings_api_mail_mail_types() {
  $token_vars = array(
    'site' => array(
      'type' => 'site',
      'label' => t('Site information'),
      'description' => t('Site-wide settings and other global information.'),
    ),
    'line_item' => array(
      'label' => t('Line item'),
      'type' => 'commerce_line_item',
    ),
    'order' => array(
      'label' => t('Order'),
      'type' => 'commerce_order',
    ),
    'user' => array(
      'label' => t('Order owner'),
      'type' => 'user',
    ),
    'product' => array(
      'label' => t('Product'),
      'type' => 'commerce_product'
    )
  );
  
  return array(
    'admin_refer' => array(
      'title' => t('Admin referral'),
      'description' => t('Received by administrators when user referral triggered'),
      'token variables' => $token_vars,
      'tokenize callback' => 'token_callback',
      'attach' => 'line_item',
      'default value' => '',  
      'weight' => 3
    )
  );
}
