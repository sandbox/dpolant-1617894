<?php

/*
 * Entity metadata property callback: retrieve variable metadata values
 */
function settings_api_entity_property_getter($data, $options, $name, $type, $info) {
  $variables = settings_api_entity_property_variables();
  
  // Only proceed if variable has a default
  if (isset($variables[$name]['default value'])) {
    $variable_def = $variables[$name];
    $value = variable_get($name, $variable_def['default value']);
    
    // Process tokenizable variables
    if (isset($variable_def['token data callback'])) {
      $callback = $variable_def['token data callback'];
      
      foreach ($variable_def['token variables'] as $token_var_name => $token_var_info) {
        // Populate a state arguments array that contains the data that the token
        // evaluator needs to replace values.
        $args = $callback($data, $token_var_name, $variable_def);
        if (!empty($args)) {
          $state_args[$token_var_name] = $args;
        }
      }
      
      // Tokenize the placeholder text
      $value = settings_api_tokenize($value, $name, $state_args);
    }
    
    return $value;
  }
}

/**
 * Fetch variable definitions
 */
function settings_api_entity_property_variables() {
  // First check the static cache for an variables array.
  $variables = &drupal_static(__FUNCTION__);

  // If it did not exist, fetch the variables now.
  if (!isset($variables)) {
    $variables = module_invoke_all('settings_api_variable_properties');

    // Give other modules a chance to alter the variables.
    drupal_alter('settings_api_variable_properties', $variables);
  }
  
  return $variables;
}


/**
 * Returns the pseudo RulesState for notification variables
 * 
 * @param array $arguments
 *  List of arguments with keys corresponding to a variable's "token variables"
 *  keys
 * 
 * @param string $variable_name
 *  Name of the settings variable
 * 
 * @return RulesState 
 */
function _settings_api_var_state($arguments, $variable_name) {
  $variables = settings_api_entity_property_variables();
  if (!empty($variables[$variable_name]['token variables'])) {
    $var_info = $variables[$variable_name]['token variables'];
  }
  else {
    return;
  }
  
  $state = new RulesState;
  foreach ($arguments as $name => $value) {
    $name = str_replace('-', '_', $name);
    if (isset($var_info[$name])) {
      $state->addVariable($name, $value, $var_info[$name]);
    }
  }

  return $state;
}

/**
 * Evalutes rules state tokens since RulesTokenEvaluator evaluate is not a static function
 * @see RulesTokenEvaluator::evaluate()
 */
function settings_api_rules_token_evaluate($text, $options, RulesState $state) {
   module_load_include('inc', 'rules', 'modules/system.eval');

  $var_info = $state->varInfo();
  $options += array('sanitize' => FALSE);

  $replacements = array();
  $data = array();
  // We also support replacing tokens in a list of textual values.
  $whole_text = is_array($text) ? implode('', $text) : $text;
  foreach (token_scan($whole_text) as $var_name => $tokens) {
    $var_name = str_replace('-', '_', $var_name);
    if (isset($var_info[$var_name]) && ($token_type = _rules_system_token_map_type($var_info[$var_name]['type']))) {
      // We have to key $data with the type token uses for the variable.
      $data = rules_unwrap_data(array($token_type => $state->get($var_name)), array($token_type => $var_info[$var_name]));
      $replacements += token_generate($token_type, $tokens, $data, $options);
    }
    else {
      $replacements += token_generate($var_name, $tokens, array(), $options);
    }

    if (!empty($options['clear'])) {
      $replacements += array_fill_keys($tokens, '');
    }
  }

  // Optionally clean the list of replacement values.
  if (!empty($options['callback']) && function_exists($options['callback'])) {
    $function = $options['callback'];
    $function($replacements, $data, $options);
  }

  // Actually apply the replacements.
  $tokens = array_keys($replacements);
  $values = array_values($replacements);
  if (is_array($text)) {
    foreach ($text as $i => $text_item) {
      $text[$i] = str_replace($tokens, $values, $text_item);
    }
    return $text;
  }
  return str_replace($tokens, $values, $text);
}

/**
 * Returns a tokenized string for a variable, its arguments and its text.
 * 
 * @param string $text
 *  Template that has tokens
 * 
 * @param string $variable_name
 *  Name of the variable being tokenized
 * 
 * @param array $arguments
 *  List of arguments with keys matching the keys of the variable's "token 
 *  variables" element
 * 
 * @return string
 *  Tokenized text
 */
function settings_api_tokenize($text, $variable_name, $arguments = array(), $sanitize = TRUE) {
  if (!empty($text)) {
    $replace_options = array('language' => language_default(), 'sanitize' => $sanitize, 'clear' => TRUE);

    $state = _settings_api_var_state($arguments, $variable_name);
    if (!empty($state)) {
      return settings_api_rules_token_evaluate($text, $replace_options, $state);
    }

    // Fallback to token replace
    return token_replace($text, $variables, $replace_options);
  }

  return $text;
}
