<?php

/*
 * Implements hook_entity_property_info_alter().
 */
function settings_api_entity_property_info_alter(&$info) {
 
  // Create site metadata properties for specific variables
  foreach (settings_api_entity_property_variables() as $name => $variable) {
    if (isset($info[$variable['attach']])) {
      $info[$variable['attach']]['properties'][$name] = array(
        'label' => isset($variable['label']) ? $variable['label'] : $name,
        'description' => isset($variable['description']) ? $variable['description'] : '',
        'type' => $variable['type'],
        'getter callback'=> 'settings_api_entity_property_getter',
      );
    }
  }
}
